# Java Spring Application for Visualisation of Building Clustering


**Technologies:**

- JavaSE-10 (java-11-openjdk) also compatible with Java 1.8
- Java Spring Framework 5.1.3
- Tomcat 9
- HTML Admin Template https://blackrockdigital.github.io/startbootstrap-sb-admin-2/pages/index.html
- D3.js based Parallel Coordinates - Credits: Jason Davies https://bl.ocks.org/jasondavies/1341281



**Installation:**

in eclipse:

extensions:
- web, xml, javaee extension
- jst server adapter
- m2e wtp

should be installed (Help -> install new Software)

set up Project:

* File -> import -> import -> Git -> Project from Git -> clone uri -> https://gitlab.com/geovisual/parallelcoordinates.git -> next -> next
-> Pfad eingeben -> import as general project -> next -> important! Project name: Geo -> finish

* rightclick on project -> configure -> convert to maven project

* rightclick on project -> Build Path -> Build Path configure -> Project Facets -> link -> Java,Dynamic Web Module,JavaScript -> apply and close

* Servers tab -> right click -> New Server -> Tomcat V9 Server -> next -> choose folder of Tomcat 9 Installation -> next -> add Geo project -> finish

* right click on project -> Properties -> Deployment Assembly -> add -> Java Build Path Entries -> select Maven Dependencies -> finish

* right click on project -> run as -> Maven build... -> Goals: clean install -> run

	if maven asks if there's a JDK instead of a JRE: 
	Window -> Preferences -> Java -> Installed JREs -> edit -> insteaad of .../Java/jre_1.8.. pick .../Java/jdk_1.8..

	if maven can't find Java at all:
	rightclick -> Properties -> BuildPath -> Libraries -> edit faulty Java Version and select System Standard Version instead. important! don't forget to change pom.xml accordingly (see comments)

* right click on project -> Properties -> buildpath -> Source -> add Folder -> resources -> apply and close

* right click on Servers -> start

-> localhost:8080/Geo
