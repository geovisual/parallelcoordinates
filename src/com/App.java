package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class App {
	private static Logger logger = LogManager.getLogger(App.class.getName());

	private ArrayList<ArrayList<String>> clus0;
	private ArrayList<ArrayList<String>> clus1;
	private ArrayList<ArrayList<String>> clas;
	private ArrayList<ArrayList<String>> cluster1;
	private ArrayList<ArrayList<String>> cluster2;
	private ArrayList<ArrayList<String>> cluster3;
	private ArrayList<ArrayList<String>> cluster4;
	private ArrayList<ArrayList<String>> cluster5;
	private ArrayList<ArrayList<String>> cluster6;
	private ArrayList<ArrayList<String>> class0;
	private ArrayList<ArrayList<String>> class1;
	private ArrayList<ArrayList<String>> class2;
	private ArrayList<ArrayList<String>> class3;
	private ArrayList<ArrayList<String>> clustering1;
	private ArrayList<ArrayList<String>> clustering2;
	private ArrayList<ArrayList<String>> stuggi;

	/**
	 * prepares data structures for the frontend presentation. each cluster or
	 * classification will be a different color within the chart, so they have to be
	 * extracted from csv files first
	 */
	@PostConstruct
	public void init() {

		clustering1 = readCsv("./Clustering1.csv");
		clustering2 = readCsv("./Clustering2.csv");
		stuggi = readCsv("./StuttgartData.csv");

		// remove unnecessary attributes original id, db id, fid, epoque
		for (int i = 0; i < stuggi.size(); i++) {
			stuggi.get(i).remove(19);
			stuggi.get(i).remove(11);
			stuggi.get(i).remove(10);
			stuggi.get(i).remove(9);
			stuggi.get(i).remove(1);
		}

		// remove unnecessary attributes volume,heightBBOX,floor_area
		for (int i = 0; i < clustering1.size(); i++) {
			clustering1.get(i).remove(5);
			clustering1.get(i).remove(3);
			clustering1.get(i).remove(2);
		}

		// remove unnecessary attributes Run number (fixed),volume,heightBBOX,floor_area
		for (int i = 0; i < clustering2.size(); i++) {
			clustering2.get(i).remove(6);
			clustering2.get(i).remove(4);
			clustering2.get(i).remove(3);
			clustering2.get(i).remove(1);
		}

		cluster1 = buildCluster(new ArrayList<ArrayList<String>>(), 1, 1);
		cluster2 = buildCluster(new ArrayList<ArrayList<String>>(), 2, 2);
		cluster3 = buildCluster(new ArrayList<ArrayList<String>>(), 1, 3);
		cluster4 = buildCluster(new ArrayList<ArrayList<String>>(), 2, 4);
		cluster5 = buildCluster(new ArrayList<ArrayList<String>>(), 3, 5);
		cluster6 = buildCluster(new ArrayList<ArrayList<String>>(), 4, 6);
		class0 = buildClasses(new ArrayList<ArrayList<String>>(), 0);
		class1 = buildClasses(new ArrayList<ArrayList<String>>(), 1);
		class2 = buildClasses(new ArrayList<ArrayList<String>>(), 2);
		class3 = buildClasses(new ArrayList<ArrayList<String>>(), 3);

		clus0 = (ArrayList<ArrayList<String>>) Stream.of(cluster1, cluster2).flatMap(x -> x.stream())
				.collect(Collectors.toList());

		clus1 = (ArrayList<ArrayList<String>>) Stream.of(cluster3, cluster4, cluster5, cluster6)
				.flatMap(x -> x.stream()).collect(Collectors.toList());

		clas = (ArrayList<ArrayList<String>>) Stream.of(class0, class1, class2, class3).flatMap(x -> x.stream())
				.collect(Collectors.toList());

		// remove false results
		for (int i = 0; i < clus1.size(); i++) {
			if (Double.parseDouble(clus1.get(i).get(2)) > 10) {
				clus1.remove(i);
			}
		}
		addHeaderCluster(clus0);
		addHeaderCluster(clus1);
		addHeaderClass(clas);

	}

	public ArrayList<ArrayList<String>> readCsv(String file) {

		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		String line = "";
		String cvsSplitBy = ",";

		ClassLoader classLoader = getClass().getClassLoader();
		InputStream is = classLoader.getResourceAsStream(file);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
			// br.readLine();
			while ((line = br.readLine()) != null) {

				// use comma as separator
				ArrayList<String> row = new ArrayList<String>(Arrays.asList(line.split(cvsSplitBy)));

				result.add(row);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;

	}

	public ArrayList<ArrayList<String>> buildCluster(ArrayList<ArrayList<String>> cluster, int clusterNr,
			int actualNr) {

		// get cluster
		if (actualNr <= 2) {
			cluster = (ArrayList<ArrayList<String>>) clustering1.stream()
					.filter(x -> x.get(1).equals(String.valueOf(clusterNr))).collect(Collectors.toList());
			cluster.stream().forEach(x -> x.set(1, String.valueOf(actualNr)));
		} else {
			cluster = (ArrayList<ArrayList<String>>) clustering2.stream()
					.filter(x -> x.get(1).equals(String.valueOf(clusterNr))).collect(Collectors.toList());
			cluster.stream().forEach(x -> x.set(1, String.valueOf(actualNr)));
		}

		// merge stuggi + clustering
		cluster.stream().forEach(y -> stuggi.stream().forEach(x -> {
			if (x.get(0).equals(String.valueOf(y.get(0)))) {
				y.addAll(x);
				y.remove(3); // remove id
				y.remove(0); // remove duplicate id
			}
		}));

		// sort
		cluster.stream().forEach(x -> {
			ArrayList<String> buff = new ArrayList<>();
			buff.add(x.get(0)); // Cluster
			buff.add(x.get(6)); // Baujahr
			buff.add(x.get(8)); // UG
			buff.add(x.get(3)); // Vol
			buff.add(x.get(4)); // Bodenfläche
			buff.add(x.get(7)); // Höhe
			buff.add(x.get(5)); // Dachfläche
			buff.add(x.get(14)); // Wandfläche
			buff.add(x.get(9)); // Wandfläche hell
			buff.add(x.get(10)); // Wandfläche dunkel
			buff.add(x.get(11)); // Dach:Boden
			buff.add(x.get(12)); // Volumen:Boden
			buff.add(x.get(13)); // Hell:dunkel
			buff.add(x.get(15)); // Boden:Höhe
			buff.add(x.get(1)); // Sonnenfläche:Wand
			buff.add(x.get(2)); // Klassifikation
			x.clear();
			x.addAll(buff);
		});
		return cluster;

	}

	public ArrayList<ArrayList<String>> buildClasses(ArrayList<ArrayList<String>> cluster, int classNr) {

		cluster = (ArrayList<ArrayList<String>>) stuggi.stream().filter(x -> x.get(1).equals(String.valueOf(classNr)))
				.collect(Collectors.toList());

		cluster.stream().forEach(x -> {
			x.remove(0); // remove id
		});

		// sort
		cluster.stream().forEach(x -> {
			ArrayList<String> buff = new ArrayList<>();
			buff.add(x.get(0)); // Klassifikation
			buff.add(x.get(4)); // Baujahr
			buff.add(x.get(6)); // UG
			buff.add(x.get(1)); // Vol
			buff.add(x.get(2)); // Bodenfläche
			buff.add(x.get(5)); // Höhe
			buff.add(x.get(3)); // Dachfläche
			buff.add(x.get(12)); // Wandfläche
			buff.add(x.get(7)); // Wandfläche hell
			buff.add(x.get(8)); // Wandfläche dunkel
			buff.add(x.get(9)); // Dach:Boden
			buff.add(x.get(10)); // Volumen:Boden
			buff.add(x.get(11)); // Hell:dunkel
			buff.add(x.get(13)); // Boden:Höhe
			x.clear();
			x.addAll(buff);
		});
		return cluster;

	}

	public void addHeaderCluster(ArrayList<ArrayList<String>> cluster) {
		ArrayList<String> header = new ArrayList<>();
		header.add("Cluster");
		header.add("Baujahr");
		header.add("Untergeschosse");
		header.add("Volumen");
		header.add("Bodenfläche");
		header.add("Höhe");
		header.add("Dachfläche");
		header.add("Wandfläche");
		header.add("Wandfläche hell");
		header.add("Wandfläche dunkel");
		header.add("Dach:Boden");
		header.add("Volumen:Boden");
		header.add("Hell:Dunkel");
		header.add("Boden:Höhe");
		header.add("Sonnenfläche:Wand");
		header.add("Klassifikation");
		cluster.add(0, header);
	}
	
	public void addHeaderClass(ArrayList<ArrayList<String>> cluster) {
		ArrayList<String> header = new ArrayList<>();
		header.add("Klassifikation");
		header.add("Baujahr");
		header.add("Untergeschosse");
		header.add("Volumen");
		header.add("Bodenfläche");
		header.add("Höhe");
		header.add("Dachfläche");
		header.add("Wandfläche");
		header.add("Wandfläche hell");
		header.add("Wandfläche dunkel");
		header.add("Dach:Boden");
		header.add("Volumen:Boden");
		header.add("Hell:Dunkel");
		header.add("Boden:Höhe");
		cluster.add(0, header);
	}

	@RequestMapping(value = "/clus0", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ArrayList<ArrayList<String>> getCluster1() {
logger.info("heeeeelllooooooo");
		return clus0;

	}

	@RequestMapping(value = "/clus1", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ArrayList<ArrayList<String>> getCluster2() {

		return clus1;

	}

	@RequestMapping(value = "/clas", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ArrayList<ArrayList<String>> getClas() {

		return clas;

	}

}
